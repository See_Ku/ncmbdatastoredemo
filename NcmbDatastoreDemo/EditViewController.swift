//
//  EditViewController.swift
//  NcmbDatastoreDemo
//
//  Created by See.Ku on 2016/02/18.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

class EditViewController: UIViewController, UITextFieldDelegate {

	// /////////////////////////////////////////////////////////////
	// MARK: - Nifty Cloud Mobile Backend

	var editData: StoreData?

	/// データを更新
	func updateData() {
		guard let data = editData else { return }

		// 非同期で保存（更新）
		data.saveInBackgroundWithBlock() { error in
			if let error = error {
				print("update error: \(error)")
			} else {
				print("update ok: ")
			}
		}
	}

	/// データを削除
	func deleteData() {
		guard let data = editData else { return }

		// 非同期で削除
		data.deleteInBackgroundWithBlock() { error in
			if let error = error {
				print("delete error: \(error)")
			} else {
				print("delete ok: ")
				self.editData = nil
				self.configureView()
			}
		}
	}
	

	// /////////////////////////////////////////////////////////////
	// MARK: - Interface

	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var numberField: UITextField!
	@IBOutlet weak var objectIdField: UITextField!


	@IBAction func onUpdate(sender: AnyObject) {
		updateData()
	}

	@IBAction func onDelete(sender: AnyObject) {
		deleteData()

	}

	/// dataをまとめて画面に反映
	func configureView() {
		if let data = editData {
			nameField.text = data.name
			numberField.text = String(data.number)
			objectIdField.text = data.objectId
		} else {
			nameField.text = ""
			numberField.text = ""
			objectIdField.text = ""
		}
	}

	/// 画面の情報をまとめてdataに反映
	func configureData() {
		guard let data = editData else { return }

		data.name = nameField.text

		let text = numberField.text ?? "0"
		data.number = Int(text) ?? 0
	}


	// /////////////////////////////////////////////////////////////
	// MARK: - TextField

	func textFieldShouldReturn(textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		configureData()
		configureView()
		return true
	}


	// /////////////////////////////////////////////////////////////
	// MARK: - other

    override func viewDidLoad() {
        super.viewDidLoad()

		nameField.delegate = self
		numberField.delegate = self
    }

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)

		configureView()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
