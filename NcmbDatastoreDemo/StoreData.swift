//
//  StoreData.swift
//  NcmbDatastoreDemo
//
//  Created by See.Ku on 2016/02/18.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import NCMB

@objc(StoreData)
class StoreData: NCMBObject, NCMBSubclassing {

	// /////////////////////////////////////////////////////////////
	// MARK: - NCMBSubclassing

	/// mobile backend上のクラス名を返却する
	static func ncmbClassName() -> String! {
		return "StoreData"
	}

	// /////////////////////////////////////////////////////////////
	// MARK: - property

	/// 文字列型のプロパティ
	var name: String! {
		get {
			if let str = objectForKey("name") as? String {
				return str
			} else {
				return ""
			}
		}

		set {
			setObject(newValue, forKey: "name")
		}
	}

	/// 数値型のプロパティ
	var number: Int {
		get {
			if let num = objectForKey("number") as? NSNumber {
				return num.integerValue
			} else {
				return 0
			}
		}

		set {
			setObject(NSNumber(integer: newValue), forKey: "number")
		}
	}
}
