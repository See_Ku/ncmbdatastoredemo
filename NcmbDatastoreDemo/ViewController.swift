//
//  ViewController.swift
//  NcmbDatastoreDemo
//
//  Created by See.Ku on 2016/02/18.
//  Copyright (c) 2016 AxeRoad. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

	// /////////////////////////////////////////////////////////////
	// MARK: - Nifty Cloud Mobile Backend

	/// 新しいデータを作成
	func createData() {

		// 新しいデータを作成
		let data = StoreData.object() as! StoreData
		data.name = "---"
		data.number = 0

		// 非同期で保存
		data.saveInBackgroundWithBlock() { error in
			if let error = error {
				print("create error: \(error)")
				return
			}

			print("create ok: ")

			// 画面を更新
			let main = dispatch_get_main_queue()
			dispatch_async(main) {
				self.readDataSync()
				self.tableView.reloadData()
			}
		}
	}

	var dataArray: [StoreData]?

	/// データを読み込み　※同期処理
	func readDataSync() {
		let query = StoreData.query()
		query.limit = 20

		do {
			dataArray = try query.findObjects() as? [StoreData]
			print("read ok: \(dataArray?.count)")

		} catch {
			print("read error: \(error)")
			dataArray = nil
		}
	}


	// /////////////////////////////////////////////////////////////
	// MARK: - Interface

	@IBOutlet weak var tableView: UITableView!

	@IBAction func onAddData(sender: AnyObject) {
		createData()
	}


	// /////////////////////////////////////////////////////////////
	// MARK: - TableView

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let ar = dataArray {
			return ar.count
		} else {
			return 1
		}
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
		if let dataArray = dataArray {
			let data = dataArray[indexPath.row]
			cell.textLabel?.text = "name: \(data.name) number: \(data.number)"
			cell.detailTextLabel?.text = "id: \(data.objectId)"
		} else {
			cell.textLabel?.text = "No data"
		}
		return cell
	}

	
	// /////////////////////////////////////////////////////////////
	// MARK: - other

	override func viewDidLoad() {
		super.viewDidLoad()

		tableView.dataSource = self
		tableView.delegate = self

		navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .Plain, target: nil, action: nil)
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)

		readDataSync()
		tableView.reloadData()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		guard let indexPath = self.tableView.indexPathForSelectedRow else { return }
		guard let data = dataArray?[indexPath.row] else { return }
		guard let vc = segue.destinationViewController as? EditViewController else { return }

		vc.editData = data
	}
}

