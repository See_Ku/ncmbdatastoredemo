# NcmbDatastoreDemo

これは、Nifty Cloud Mobile Backendでデータストアを使うときの基本的な操作について、簡単にまとめたデモアプリです。

mBaaSでサーバー開発不要！ | ニフティクラウド mobile backend  
http://mb.cloud.nifty.com/


### 内容

主に、以下のような項目についてまとめてあります。

* オブジェクトのサブクラス化
* データセットへの保存
* データセットから読み込み（同期処理）
* データセットの更新
* データセットの削除

UITableViewやUITextFieldも使ってますが、そこは本題ではないので少し適当です。


### 使い方

Nifty Cloud Mobile Backendを使うのに必要な、アプリケーションキーとクライアントキーは入っていません。

『Keys-dummy.plist』を『Keys.plist』と言う名前でコピーして、プロパティリストのそれらしいところに、それぞれのキーを登録してください。

あとは、CocoaPods経由でNifty Cloud Mobile BackendのSDKを入れるだけで、コンパイル出来るはずです。たぶん。


### ライセンス

ソースコードのライセンスは CC0 とします。

Creative Commons — CC0 1.0 Universal  
http://creativecommons.org/publicdomain/zero/1.0/

